

pub const PACKET_SIZE: usize = 1400;

pub struct DataPacket {
/// The idea here is to provide a delta from the last ack'd sequence
	sequence: u64, // (8 bytes) the sequence id for this update
	data: [u8; PACKET_SIZE]
}

impl DataPacket {
	pub fn new(sequence: u64, data: [u8; PACKET_SIZE]) -> DataPacket {
		DataPacket{ sequence: sequence, data: data }
	}
}

// Clients respond to server state with an Ack
pub struct Ack {
	sequence: u64,
	client_id: u64
}

impl Ack {
	pub fn new(sequence: u64, client_id: u64) -> Ack {
		Ack{ sequence: sequence, client_id: client_id }
	}
}

