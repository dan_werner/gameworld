extern crate std;
extern crate time;

extern crate rmp as msgpack;
extern crate rustc_serialize;
/*
use self::rustc_serialize::Encodable;
use self::rustc_serialize::Decodable;

use self::msgpack::Encoder;
use self::msgpack::Decoder;
*/


use std::vec::Vec;
use std::fmt;

pub struct Entity {
	name: String,
	location: Point
}

impl Entity {
	pub fn new(name:String, loc:Point) -> Entity {
		Entity{ name: name, location: loc }
	}
}

// TODO: look at the #derive attribute to see if we can simplify this 
impl fmt::Display for Entity {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "Entity(name: '{}', location:{:?})", self.name, self.location)
	}
}

impl fmt::Debug for Entity {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		fmt::Display::fmt(self, f)
	}
}

pub struct Point {
	x: f64,
	y: f64,
	z: f64
}

impl Point {
	pub fn new(x: f64, y: f64, z: f64) -> Point {
		Point{x: x, y: y, z: z}
	}

	pub fn lerp(&self, target:Point, amount:f32) {
		println!("{:?} {}", target, amount);
		// TODO: fill me in
	}

	pub fn x(&self) -> f64 { self.x }
	pub fn y(&self) -> f64 { self.y }
	pub fn z(&self) -> f64 { self.z }
}

impl fmt::Display for Point {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "Point(x:{}, y:{}, z:{})", self.x, self.y, self.z)
	}
}

impl fmt::Debug for Point {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		fmt::Display::fmt(self, f)
	}
}


// A chunk of a world, fixed size?
pub struct Chunk {
	origin: Point,
	last_update: time::Timespec
}

impl Chunk {
	pub fn new(p: Point) -> Chunk {
		Chunk{ origin: p, last_update: time::now_utc().to_timespec() }
	}
}

impl fmt::Display for Chunk {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "Chunk(origin: {:?}, last_updated:{:?})", self.origin, self.last_update)
	}
}

impl fmt::Debug for Chunk {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		fmt::Display::fmt(self, f)
	}
}

// A world, comprised of chunks
pub struct World {
	entities: Vec<Entity>,
	chunks: Vec<Chunk>
}

impl World {
	pub fn new(entities:Vec<Entity>, chunks: Vec<Chunk>) -> World {
		World{ entities: entities, chunks: chunks }
	}
}

impl fmt::Display for World {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "World(entities: {:?}, chunks:{:?})", self.entities, self.chunks)
	}
}

impl fmt::Debug for World {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		fmt::Display::fmt(self, f)
	}
}
