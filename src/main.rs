extern crate gameworld;
extern crate time;

const SEND_AMT:usize = 1400;

use gameworld::net::transport::Message;
use gameworld::net::transport::SocketActor;

fn main() {

	let mut actor1 = SocketActor::new("localhost:9090", "localhost:9091", "localhost:9093");
	let mut actor2 = SocketActor::new("localhost:9092", "localhost:9093", "localhost:9091");

	let mut actor2_bytes_recv:usize = 0;
	let mut actor1_bytes_recv:usize = 0;
	let current_time: time::Timespec = time::now_utc().to_timespec();
	let limit = time::Duration::seconds(5);

	loop {
		let iteration_time = time::now_utc().to_timespec() - current_time;

		std::thread::sleep_ms(1);

		match actor1.try_recv() {
			Some(val) => {
				match val {
					Message::Ready => {
						println!("SocketActor 1 ready.");
						actor1.send(Message::Data([4;SEND_AMT],SEND_AMT));
					},
					Message::Data(buf, amt) => {
						actor1_bytes_recv += amt;
						actor2.send(Message::Data(buf, amt));
						if amt != SEND_AMT {
							println!("1 Received only {} of {}", amt, SEND_AMT);
						}
					}
				}
			},
			None => {}
		}

		match actor2.try_recv() {
			Some(val) => {
				match val {
					Message::Ready => {
						println!("SocketActor 2 ready.");
						actor2.send(Message::Data([3;SEND_AMT],SEND_AMT));
					},
					Message::Data(buf, amt) => {
						actor2_bytes_recv += amt;
						actor1.send(Message::Data(buf, amt));
						if amt != SEND_AMT {
							println!("2 Received only {} of {}", amt, SEND_AMT);
						}
					}
				}
			},
			None => {}
		}

		if iteration_time >= limit {
			break;
		}
	}
	println!("Transferred a total of {} bytes to actor1 and {} bytes to actor2 in {:?}", 
					 actor1_bytes_recv, actor2_bytes_recv, time::now_utc().to_timespec() - current_time);
}
