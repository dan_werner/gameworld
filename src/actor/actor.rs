
use std::thread;
use std::sync::mpsc::channel;
use std::sync::mpsc::{ Receiver, Sender };


struct Message<T: Send> {
	//from: &'a Actor<T>,
	msg: T
}

impl <T:Send> Message <T> {
	fn new(&self, msg: T) -> Message<T> {
		Message { msg: msg }
	}
}

pub struct Actor<T:Send> {
	recv_thread: thread::JoinHandle<i32>,
	send_thread: thread::JoinHandle<i32>,
	rx: Receiver<T>,
	tx: Sender<T>,
	mailbox: Vec<Message<T>>
}

impl <'a, T:Send> Actor<T> {
	pub fn new(receive: &Fn(T) -> () ) -> Actor<T> {
		let mailbox: Vec<Message<T>> = Vec::new();
		let (send_tx, send_rx) = channel();
		let (recv_tx, recv_rx) = channel();
		let send_thread: thread::JoinHandle<i32> = thread::spawn(move || {
			loop {
				match send_rx.recv() {
					// blindly reusing Message here
					Ok(message) => { mailbox.push(message); },
					Err(_) => {}
				}
			}
			0
		});

		// recv thread
		let recv_thread: thread::JoinHandle<i32> = thread::spawn(move|| {
			loop {
				// pop from mailbox, run receive
			}
			0
		});


		Actor { 
			send_thread: send_thread,
			recv_thread: recv_thread,
			rx: recv_rx,
			tx: send_tx,
			mailbox: mailbox
		}
	}

	pub fn send(&mut self, data: Message<T>) {
		self.tx.send(data);
	}

	pub fn try_recv(&self) -> Option<Message<T>> {
		match self.rx.try_recv() {
			Ok(val) => Some(val),
			Err(_) => None
		}
	}
}

#[cfg(test)]
mod tests {
	use super::Actor;

	#[test]
	fn actor_works() {
		let blah = Actor::new();
	}
}
